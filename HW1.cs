﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ДЗ1В4
{
    class Program
    {
        static void Main(string[] args)
        {
            int side = 0, filstr = 5, crwidth1, crwidth2, colstar, thrpart, timerez, countstr, countsym, countcolsym = 0, timerez2;
            bool cornum = false, ansou = true, corans = false;
            string ansoustr = "";
            char symbackgr, symcro;
            do
            {
                Console.Write("Введите сторону флага от 21 до 41 -> ");
                do
                {
                    try
                    {
                        side = Convert.ToInt32(Console.ReadLine());
                    }
                    catch (Exception)
                    {
                        Console.Write("Число введенно не правильно введите правильное число в указанном диапазоне -> ");
                        continue;
                    }
                    if (side < 21 || side > 41)
                    {
                        Console.Write("Число введенно не правильно введите правильное число в указанном диапазоне -> ");
                        continue;
                    }
                    cornum = true;
                }
                while (cornum == false);
                Console.Write("Введите символ которым рисовать фон -> ");
                symbackgr = Convert.ToChar(Console.ReadLine());
                Console.Write("Введите символ которым рисовать крестик -> ");
                symcro = Convert.ToChar(Console.ReadLine());
                crwidth1 = side - (filstr * 2);
                crwidth2 = crwidth1 / 3;
                timerez = crwidth2 * 2;
                thrpart = crwidth1 - timerez;
                timerez = side - crwidth2;
                colstar = timerez / 2;
                Console.WriteLine("");
                for (countstr = 1; countstr <= filstr; countstr++)
                {
                    for (countsym = 1; countsym <= side; countsym++)
                    {
                        if (countsym == side)
                        {
                            Console.WriteLine(symbackgr);
                            break;
                        }
                        Console.Write(symbackgr);
                    }
                }
                for (countstr = 1; countstr <= crwidth2; countstr++)
                {
                    for (countsym = 1; countsym <= colstar; countsym++)
                    {
                        Console.Write(symbackgr);
                        countcolsym++;
                    }
                    for (countsym = 1; countsym <= crwidth2; countsym++)
                    {
                        Console.Write(symcro);
                        countcolsym++;
                    }
                    for (countsym = 1; countsym <= colstar; countsym++)
                    {
                        Console.Write(symbackgr);
                        countcolsym++;
                        if (countsym == colstar && countcolsym == side - 1)
                        {
                            Console.WriteLine(symbackgr);
                            countcolsym = 0;
                            break;
                        }
                        else
                            if (countsym == colstar && countcolsym < side - 1)
                            {
                                timerez2 = side - countcolsym;
                                countsym = countsym - timerez2;
                                continue;
                            }
                            else
                                if (countsym == colstar && countcolsym == side)
                                {
                                    Console.WriteLine();
                                    countcolsym = 0;
                                    break;
                                }
                    }
                }
                for (countstr = 1; countstr <= crwidth2; countstr++)
                {
                    for (countsym = 1; countsym <= filstr; countsym++)
                    {
                        Console.Write(symbackgr);
                    }
                    for (countsym = 1; countsym <= crwidth1; countsym++)
                    {
                        Console.Write(symcro);
                    }
                    for (countsym = 1; countsym <= filstr; countsym++)
                    {
                        if (countsym == filstr)
                        {
                            Console.WriteLine(symbackgr);
                            break;
                        }
                        Console.Write(symbackgr);
                    }
                }
                for (countstr = 1; countstr <= thrpart; countstr++)
                {
                    for (countsym = 1; countsym <= colstar; countsym++)
                    {
                        Console.Write(symbackgr);
                        countcolsym++;
                    }
                    for (countsym = 1; countsym <= crwidth2; countsym++)
                    {
                        Console.Write(symcro);
                        countcolsym++;
                    }
                    for (countsym = 1; countsym <= colstar; countsym++)
                    {
                        Console.Write(symbackgr);
                        countcolsym++;
                        if (countsym == colstar && countcolsym == side - 1)
                        {
                            Console.WriteLine(symbackgr);
                            countcolsym = 0;
                            break;
                        }
                        else
                            if (countsym == colstar && countcolsym < side - 1)
                            {
                                timerez2 = side - countcolsym;
                                countsym = countsym - timerez2;
                                continue;
                            }
                            else
                                if (countsym == colstar && countcolsym == side)
                                {
                                    Console.WriteLine();
                                    countcolsym = 0;
                                    break;
                                }
                    }
                }
                for (countstr = 1; countstr <= filstr; countstr++)
                {
                    for (countsym = 1; countsym <= side; countsym++)
                    {
                        if (countsym == side)
                        {
                            Console.WriteLine(symbackgr);
                            break;
                        }
                        Console.Write(symbackgr);
                    }
                }
                Console.WriteLine("");
                Console.Write("Желаете продолжить работу да/нет -> ");
                do
                {
                    try
                    {
                        ansoustr = Convert.ToString(Console.ReadLine());
                    }
                    catch (Exception)
                    {
                        Console.Write("Ответ введенн не правильно введите правильный ответ - да или нет -> ");
                        continue;
                    }
                    if (ansoustr != "да" && ansoustr != "Да" && ansoustr != "нет" && ansoustr != "Нет")
                    {
                        Console.Write("Ответ введенн не правильно введите правильный ответ - да или нет -> ");
                        continue;
                    }
                    corans = true;
                }
                while (corans == false);
                corans = false;
                if (ansoustr == "нет" || ansoustr == "Нет")
                    ansou = false;
            }
            while (ansou == true);
        }
    }
}
