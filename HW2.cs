﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Search
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 1;
            Console.Write("Введите маску для файлов: ");
            string Mask = Console.ReadLine(); 
            Mask = Mask.Replace(".", @"\." /* (".", "\\.") */); 
            Mask = Mask.Replace("?", "."); 
            Mask = Mask.Replace("*", ".*"); 
            Mask = "^" + Mask + "$"; 
            Regex regMask = new Regex(Mask, RegexOptions.IgnoreCase);
            string[] driv = new string[10];
            for (int i = 0; i < 10; i++)
            {
                driv[i] = "null";
            }
            driv = Directory.GetLogicalDrives();
            Console.WriteLine("Выберите диск на котором искать ");
            foreach (string i in driv)
            {
                if (i != "null")
                {
                    try
                    {
                        Directory.GetDirectories(i);
                        Console.WriteLine("Диск {0} доступен, для выбора введите {1}", i, count);
                    }
                    catch
                    {
                        Console.WriteLine("Диск {0} недоступен", i);
                    }
                    count++;
                }
            }
            int userans = Convert.ToInt32(Console.ReadLine());
            DirectoryInfo curdriv = new DirectoryInfo(driv[userans - 1]);
            FindDelFiles(curdriv, regMask);
        }

        static void FindDelFiles(DirectoryInfo dir, Regex regMask)
        {
            string[] mfiles = new string[1];
            int countfil = 0, usrchoice = 0, usrranlow, usrranup;
            string userans;
            FileInfo [] fi = null;
            fi = dir.GetFiles();
            foreach (FileInfo f in fi)
            {
                if (regMask.IsMatch(f.Name))
                {
                    try
                    {
                        mfiles[countfil] = f.FullName;
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Array.Resize(ref mfiles, mfiles.Length + 1);
                        mfiles[countfil] = f.FullName;
                    }
                    countfil++;
                }
            }
            DirectoryInfo[] dirsub = dir.GetDirectories();
            foreach (DirectoryInfo disub in dirsub)
            {
                try
                {
                    fi = disub.GetFiles();
                    foreach (FileInfo f in fi)
                    {
                        if (regMask.IsMatch(f.Name))
                        {
                            try
                            {
                                mfiles[countfil] = f.FullName;
                            }
                            catch (IndexOutOfRangeException)
                            {
                                Array.Resize(ref mfiles, mfiles.Length + 1);
                                mfiles[countfil] = f.FullName;
                            }
                            countfil++;
                        }
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    continue;
                }
            }
            countfil = 1;
            Console.WriteLine("Найденны файлы");
            foreach (string mfil in mfiles)
            {
                if (countfil > 0 && countfil < 10)
                {
                    Console.WriteLine("{0}  {1}", countfil, mfil);
                    countfil++;
                }
                else
                {
                    Console.WriteLine("{0} {1}", countfil, mfil);
                    countfil++;
                }
            }
            Console.Write("Удалять файлы? \"да/нет\" ");
            userans = Console.ReadLine();
            if (userans == "Да" || userans == "да")
            {
                Console.WriteLine("Выберите режим удаления");
                Console.WriteLine("Удалить все файлы - 1");
                Console.WriteLine("Удалить один файл - 2");
                Console.WriteLine("Удалить несколько файлов - 3");
                usrchoice = Convert.ToInt32(Console.ReadLine());
            }
            switch (usrchoice)
            {
                case 1: foreach (string flpth in mfiles)
                        {
                            File.Delete(flpth);
                        }
                        Console.WriteLine("Все файлы удаленны");
                        break;

                case 2: Console.Write("Выберите файл(введите его номер) - ");
                        usrchoice = Convert.ToInt32(Console.ReadLine());
                        File.Delete(mfiles[usrchoice - 1]);
                        Console.WriteLine("Выбранный файл удалённ");
                        break;

                case 3: Console.Write("Выберите диапазон файлов(введите номер файла с которого начать удаление) - ");
                        usrranlow = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Выберите диапазон файлов(введите номер файла на котором закончить удаление) - ");
                        usrranup = Convert.ToInt32(Console.ReadLine());
                        for (int cnt = usrranlow - 1; cnt <= usrranup - 1; cnt++)
                        {
                            File.Delete(mfiles[cnt]);
                        }
                        Console.WriteLine("Выбранные файлы удалены");
                        break;
                default: break;
            }
        }
    }
}
