﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ООП
{
    public class Employee
    {
        public string Name;
        public string LastName;
        public string Sex;
        public int Age;
        public void outstr()
        {
            Console.WriteLine("<Employees>");
        }
    }

    public class Company
    {
        public Director[] MasDir = new Director[1];
        public void outst()
        {
            Console.WriteLine("<Company>");
        }
        public void outdir()
        {
            for (int i = 0; i < 1; i++)
            {
                MasDir[i] = new Director();
            }
            int countd = 1;
            foreach (Director i in MasDir)
            {
                Console.WriteLine("<Director " + countd + ">");
                i.manout();
                countd++;
            }
        }
    }

    public class Worker : Company
    {
        public int ID;
        public string LastName;
    }

    public class Manager : Company
    {
        public Worker[] MasWork = new Worker[4];
        public int numWorkers = 4;
        public int man;
        public int Man
        {
            get { return man; }
            set { man = value; }
        }
        public void workout()
        {
            for (int i = 0; i < 4; i++)
            {
                MasWork[i] = new Worker();
            }
            
            for (int Count = 0; Count < 4; Count++)
            {
                MasWork[Count].ID = Count + 1;
                if (man == 1)
                {
                    switch (Count)
                    {
                        case 0: MasWork[Count].LastName = "Петров"; break;
                        case 1: MasWork[Count].LastName = "Иванов"; break;
                        case 2: MasWork[Count].LastName = "Сидоров"; break;
                        case 3: MasWork[Count].LastName = "Притчин"; break;
                    }
                }
                else
                    if (man == 2)
                    {
                        switch (Count)
                        {
                            case 0: MasWork[Count].LastName = "Андреев"; break;
                            case 1: MasWork[Count].LastName = "Степанов"; break;
                            case 2: MasWork[Count].LastName = "Яковлев"; break;
                            case 3: MasWork[Count].LastName = "Пупкин"; break;
                        }
                    }
            }
            //man++;
                foreach (Worker i in MasWork)
                {
                    Console.WriteLine("<Worker " + "ID = " + i.ID + " LastName = " + i.LastName + ">");
                }
        }
    }

    public class Director : Company
    {
        public Manager[] MasMan = new Manager[2];
        public void manout()
        {
            for (int i = 0; i < 2; i++)
            {
                MasMan[i] = new Manager();
            }
            int countm = 1;
            foreach (Manager i in MasMan)
            {
                Console.WriteLine("<Manager " + countm + " numWorkers = " + i.numWorkers + ">");
                i.man = countm;
                i.workout();
                countm++;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Company Comp = new Company();
            Employee Emp = new Employee();
            Comp.outst();
            Emp.outstr();
            Comp.outdir();
        }
    }
}
