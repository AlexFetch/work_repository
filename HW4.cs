﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Дорога
{
    public class Road
    {
        public TrVehicle[] MasTr = new TrVehicle[1];
        public void init()
        {
            for (int i = 0; i < 1; i++)
            {
                MasTr[i] = new TrVehicle();
            }
        }
    }

    public class TrVehicle
    {
        public bool violLorry;
        public bool violCar;
        public bool violBicyc;
        public bool violMotorc;
    }

    public class Lorry : TrVehicle
    {
        public bool viol = true;
        public bool setviollor()
        {
            return viol;
        }
    }

    public class Car : TrVehicle
    {
        public bool viol = false;
        public bool setviolcar()
        {
            return viol;
        }
    }

    public class Bicyc : TrVehicle
    {
        public bool viol = true;
        public bool setviolbicyc()
        {
            return viol;
        }
    }

    public class Motorc : TrVehicle
    {
        public bool viol = false;
        public bool setviolmotorc()
        {
            return viol;
        }
    }

    public class Regulator
    {
        public void Work(bool viollor, bool violcar, bool violbic, bool violmotorc)
        {
            if (viollor == true)
                Console.WriteLine("Грузовик нарушил");
            if (violcar == true)
                Console.WriteLine("Легковой автомобиль нарушил");
            if (violbic == true)
                Console.WriteLine("Велосипед нарушил");
            if (violmotorc == true)
                Console.WriteLine("Мотоцикл нарушил");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string usans;
            Road Rod = new Road();
            Lorry Lor = new Lorry();
            Car Cr = new Car();
            Bicyc Bic = new Bicyc();
            Motorc Mot = new Motorc();
            Regulator Reg = new Regulator();
            Console.Write("Нарушил ли Грузовик \"да/нет\" ");
            usans = Console.ReadLine();
            if (usans == "да" || usans == "Да")
                Lor.viol = true;
            else
                if (usans == "нет" || usans == "Нет")
                    Lor.viol = false;
            Console.Write("Нарушил ли Легковой автомобиль \"да/нет\" ");
            usans = Console.ReadLine();
            if (usans == "да" || usans == "Да")
                Cr.viol = true;
            else
                if (usans == "нет" || usans == "Нет")
                    Cr.viol = false;
            Console.Write("Нарушил ли Велосипед \"да/нет\" ");
            usans = Console.ReadLine();
            if (usans == "да" || usans == "Да")
                Bic.viol = true;
            else
                if (usans == "нет" || usans == "Нет")
                    Bic.viol = false;
            Console.Write("Нарушил ли Мотоцикл \"да/нет\" ");
            usans = Console.ReadLine();
            if (usans == "да" || usans == "Да")
                Mot.viol = true;
            else
                if (usans == "нет" || usans == "Нет")
                    Mot.viol = false;
            Rod.init();
            Rod.MasTr[0].violLorry = Lor.setviollor();
            Rod.MasTr[0].violCar = Cr.setviolcar();
            Rod.MasTr[0].violBicyc = Bic.setviolbicyc();
            Rod.MasTr[0].violMotorc = Mot.setviolmotorc();
            Reg.Work(Rod.MasTr[0].violLorry, Rod.MasTr[0].violCar, Rod.MasTr[0].violBicyc, Rod.MasTr[0].violMotorc);
        }
    }
}
