﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClaWork
{
    public class cl1
    {
        public void doWork(int clwrk)
        {
            switch (clwrk)
            {
                case 1: Console.WriteLine("Class 21 is working"); break;
                case 2: Console.WriteLine("Class 22 is working"); break;
                case 3: Console.WriteLine("Class 23 is working"); break;
                default: break;
            }
        }
    }

    public class cl2
    {
        public int whtcliswrk;
        public int doWork(bool clonew, bool cltwow, bool clthrw)
        {
            if (clonew == true)
            {
                whtcliswrk = 1;
            }
            if (cltwow == true)
            {
                whtcliswrk = 2;
            }
            if (clthrw == true)
            {
                whtcliswrk = 3;
            }
            return whtcliswrk;
        }
    }

    public class cl21 : cl2
    {
        public bool working = false;
    }

    public class cl22 : cl2
    {
        public bool working = true;
    }

    public class cl23 : cl2
    {
        public bool working = false;
    }

    class Program
    {
        static void Main(string[] args)
        {
            cl1 class1 = new cl1();
            cl2 class2 = new cl2();
            cl21 class21 = new cl21();
            cl22 class22 = new cl22();
            cl23 class23 = new cl23();
            class1.doWork(class2.doWork(class21.working, class22.working, class23.working));
        }
    }
}
