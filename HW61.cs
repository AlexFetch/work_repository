﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrClas
{
    class WorkArr
    {
        private int[] arr1 = new int[10];
        public WorkArr()
        {
            for (int i = 0; i < 10; i++)
            {
                arr1[i] = 0;
            }
        }
        public int this[int i]
        {
            get
            {
                if (i < 0 || i > 9)
                    throw new IndexOutOfRangeException("Index must be between 0 and 9");
                return arr1[i];
            }
            set
            {
                if (i < 0 || i > 9)
                    throw new IndexOutOfRangeException("Index must be between 0 and 9");
                arr1[i] = value;
            }
        }
        public void ToString()
        {
            string elem;
            for (int i = 0; i < 10; i++)
            {
                elem = Convert.ToString(arr1[i]);
                switch (i)
                {
                    case 9: Console.WriteLine("[{0}]", elem); break;
                    default: Console.Write("[{0}], ", elem); break;
                }
                
            }
        }
        public void ChoiceSort()
        {
            int countstout = 1, countstin, numminel = 0;
            do
            {
                switch (countstout)
                {
                    case 1:
                        for (countstin = 0; countstin < 10; countstin++)
                        {
                            if (countstin == 0)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 0)
                        {
                            arr1[0] = arr1[0] + arr1[numminel];
                            arr1[numminel] = arr1[0] - arr1[numminel];
                            arr1[0] = arr1[0] - arr1[numminel];
                        }
                        break;
                    case 2:
                        for (countstin = 1; countstin < 10; countstin++)
                        {
                            if (countstin == 1)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 1)
                        {
                            arr1[1] = arr1[1] + arr1[numminel];
                            arr1[numminel] = arr1[1] - arr1[numminel];
                            arr1[1] = arr1[1] - arr1[numminel];
                        }
                        break;
                    case 3:
                        for (countstin = 2; countstin < 10; countstin++)
                        {
                            if (countstin == 2)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 2)
                        {
                            arr1[2] = arr1[2] + arr1[numminel];
                            arr1[numminel] = arr1[2] - arr1[numminel];
                            arr1[2] = arr1[2] - arr1[numminel];
                        }
                        break;
                    case 4:
                        for (countstin = 3; countstin < 10; countstin++)
                        {
                            if (countstin == 3)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 3)
                        {
                            arr1[3] = arr1[3] + arr1[numminel];
                            arr1[numminel] = arr1[3] - arr1[numminel];
                            arr1[3] = arr1[3] - arr1[numminel];
                        }
                        break;
                    case 5:
                        for (countstin = 4; countstin < 10; countstin++)
                        {
                            if (countstin == 4)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 4)
                        {
                            arr1[4] = arr1[4] + arr1[numminel];
                            arr1[numminel] = arr1[4] - arr1[numminel];
                            arr1[4] = arr1[4] - arr1[numminel];
                        }
                        break;
                    case 6:
                        for (countstin = 5; countstin < 10; countstin++)
                        {
                            if (countstin == 5)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 5)
                        {
                            arr1[5] = arr1[5] + arr1[numminel];
                            arr1[numminel] = arr1[5] - arr1[numminel];
                            arr1[5] = arr1[5] - arr1[numminel];
                        }
                        break;
                    case 7:
                        for (countstin = 6; countstin < 10; countstin++)
                        {
                            if (countstin == 6)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 6)
                        {
                            arr1[6] = arr1[6] + arr1[numminel];
                            arr1[numminel] = arr1[6] - arr1[numminel];
                            arr1[6] = arr1[6] - arr1[numminel];
                        }
                        break;
                    case 8:
                        for (countstin = 7; countstin < 10; countstin++)
                        {
                            if (countstin == 7)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 7)
                        {
                            arr1[7] = arr1[7] + arr1[numminel];
                            arr1[numminel] = arr1[7] - arr1[numminel];
                            arr1[7] = arr1[7] - arr1[numminel];
                        }
                        break;
                    case 9:
                        for (countstin = 8; countstin < 10; countstin++)
                        {
                            if (countstin == 8)
                                numminel = arr1[countstin];
                            else
                                if (arr1[countstin] < numminel)
                                    numminel = arr1[countstin];
                        }
                        if (numminel > 8)
                        {
                            arr1[8] = arr1[8] + arr1[numminel];
                            arr1[numminel] = arr1[8] - arr1[numminel];
                            arr1[8] = arr1[8] - arr1[numminel];
                        }
                        break;
                }
                countstout++;
            }
            while (countstout != 10);
        }
    }

    /*class IOUser
    {
        public int UsrChoice()
        {
            Console.WriteLine("Fill array please");
            
        }
    }*/

    class Program
    {
        static void Main(string[] args)
        {
            /*WorkArr Ar1 = new WorkArr();
            Console.WriteLine("Fill array please");
            for (int i = 0; i < 10; i++)
            {
                switch(i)
                {
                    case 0: Console.Write("Type {0}st element -> ", i + 1); break;
                    case 1: Console.Write("Type {0}nd element -> ", i + 1); break;
                    case 2: Console.Write("Type {0}rd element -> ", i + 1); break;
                    default : Console.Write("Type {0}th element -> ", i + 1); break;
                }
                Ar1[i] = Convert.ToInt32(Console.ReadLine());
            }*/
            
        }
    }
}
