﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicalSquare
{
    class MagicSquare3x3
    {
        public void OutSquare()
        {
            int count;
            char verline = '-', horline = '|', crline = '+', upltcr = '\u250C', uprtcr = '\u2510', lortcr = '\u2514', loltcr = '\u2518';
            string sum = "15";
            Console.WriteLine("       {0}", sum);
            for(count = 1; count<8; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(upltcr); break;
                    case 7: Console.WriteLine(uprtcr); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}2{0}7{0}6{0}{1}", horline, sum);
            for (count = 1; count < 8; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 7: Console.WriteLine(horline); break;
                    case 3:
                    case 5: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}9{0}5{0}1{0}{1}", horline, sum);
            for (count = 1; count < 8; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 7: Console.WriteLine(horline); break;
                    case 3:
                    case 5: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}4{0}3{0}8{0}{1}", horline, sum);
            for (count = 1; count < 8; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(lortcr); break;
                    case 7: Console.WriteLine(loltcr); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}{0}{0} {0}", sum);
            Console.WriteLine();
        }
    }

    class MagicSquare4x4
    {
        public void OutSquare()
        {
            int count;
            char verline = '-', horline = '|', crline = '+', upltcr = '\u250C', uprtcr = '\u2510', lortcr = '\u2514', loltcr = '\u2518';
            string sum = "34";
            Console.WriteLine("             {0}", sum);
            for (count = 1; count < 14; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(upltcr); break;
                    case 13: Console.WriteLine(uprtcr); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}1 {0}14{0}15{0}4 {0}{1}", horline, sum);
            for (count = 1; count < 14; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 13: Console.WriteLine(horline); break;
                    case 4:
                    case 7:
                    case 10: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}8 {0}11{0}10{0}5 {0}{1}", horline, sum);
            for (count = 1; count < 14; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 13: Console.WriteLine(horline); break;
                    case 4:
                    case 7:
                    case 10: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}12{0}7 {0}6 {0}9 {0}{1}", horline, sum);
            for (count = 1; count < 14; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 13: Console.WriteLine(horline); break;
                    case 4:
                    case 7:
                    case 10: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}13{0}2 {0}3 {0}16{0}{1}", horline, sum);
            for (count = 1; count < 14; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(lortcr); break;
                    case 13: Console.WriteLine(loltcr); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine(" {0} {0} {0} {0} {0}", sum);
            Console.WriteLine();
        }
    }

    class MagicSquare5x5
    {
        public void OutSquare()
        {
            int count;
            char verline = '-', horline = '|', crline = '+', upltcr = '\u250C', uprtcr = '\u2510', lortcr = '\u2514', loltcr = '\u2518';
            string sum = "65";
            Console.WriteLine("                {0}", sum);
            for (count = 1; count < 17; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(upltcr); break;
                    case 16: Console.WriteLine(uprtcr); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}3 {0}16{0}9 {0}22{0}15{0}{1}", horline, sum);
            for (count = 1; count < 17; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 16: Console.WriteLine(horline); break;
                    case 4:
                    case 7:
                    case 10:
                    case 13: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}20{0}8 {0}21{0}14{0}2 {0}{1}", horline, sum);
            for (count = 1; count < 17; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 16: Console.WriteLine(horline); break;
                    case 4:
                    case 7:
                    case 10:
                    case 13: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}7 {0}25{0}13{0}1 {0}19{0}{1}", horline, sum);
            for (count = 1; count < 17; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 16: Console.WriteLine(horline); break;
                    case 4:
                    case 7:
                    case 10:
                    case 13: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}24{0}12{0}5 {0}18{0}6 {0}{1}", horline, sum);
            for (count = 1; count < 17; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(horline); break;
                    case 16: Console.WriteLine(horline); break;
                    case 4:
                    case 7:
                    case 10:
                    case 13: Console.Write(crline); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine("{0}11{0}4 {0}17{0}10{0}23{0}{1}", horline, sum);
            for (count = 1; count < 17; count++)
            {
                switch (count)
                {
                    case 1: Console.Write(lortcr); break;
                    case 16: Console.WriteLine(loltcr); break;
                    default: Console.Write(verline); break;
                }
            }
            Console.WriteLine(" {0} {0} {0} {0} {0} {0}", sum);
            Console.WriteLine();
        }
    }

    class InputOutput
    {
        public int IOText()
        {
            int side = 0;
            bool corrans = false;
            Console.WriteLine("MagicalSquare 1.0");
            Console.WriteLine();
            do
            {
                Console.Write("Type side of square from 3 to 5 please -> ");
                try
                {
                    side = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("The symbol you typed is not a number, please type a number.");
                    continue;
                }
                if (side < 3 || side > 5)
                {
                    Console.WriteLine("The number you typed is out of acceptable range, please type correct number.");
                    continue;
                }
                corrans = true;
            }
            while (corrans == false);
            Console.WriteLine();
            return side;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MagicSquare3x3 Ms3 = new MagicSquare3x3();
            MagicSquare4x4 Ms4 = new MagicSquare4x4();
            MagicSquare5x5 Ms5 = new MagicSquare5x5();
            InputOutput IO = new InputOutput();
            switch (IO.IOText())
            {
                case 3: Ms3.OutSquare(); break;
                case 4: Ms4.OutSquare(); break;
                case 5: Ms5.OutSquare(); break;
            }
        }
    }
}
