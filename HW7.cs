﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    public sealed class Single
    {
        static readonly Single myIn = new Single();
        static Single() { }
        Single() { }

        public static Single MyIn
        {
            get
            {
                return myIn;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
