﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deleg
{
    class MatFunc
    {
        public void Sin()
        {
            double rad, rezfun;
            Console.WriteLine("Sinuses");
            Console.WriteLine();
            for (int i = 0; i < 91; i++)
            {
                rad = i * 3.14 / 180;
                rezfun = Math.Sin(rad);
                Console.Write("Sin {0} = ", i);
                Console.WriteLine("{0:f4}", rezfun);
            }
            Console.WriteLine();
        }

        public void Cos()
        {
            double rad, rezfun;
            Console.WriteLine("Cosinuses");
            Console.WriteLine();
            for (int i = 0; i < 91; i++)
            {
                rad = i * 3.14 / 180;
                rezfun = Math.Cos(rad);
                Console.Write("Cos {0} = ", i);
                Console.WriteLine("{0:f4}", rezfun);
            }
            Console.WriteLine();
        }

        public void Tan()
        {
            double rad, rezfun;
            Console.WriteLine("Tangenses");
            Console.WriteLine();
            for (int i = 0; i < 91; i++)
            {
                rad = i * 3.14 / 180;
                rezfun = Math.Tan(rad);
                Console.Write("Tan {0} = ", i);
                Console.WriteLine("{0:f4}", rezfun);
            }
            Console.WriteLine();
        }

        public void Cotan()
        {
            double rad, rezfun;
            Console.WriteLine("Cotangenses");
            Console.WriteLine();
            for (int i = 0; i < 91; i++)
            {
                rad = i * 3.14 / 180;
                rezfun = 1/Math.Tan(rad);
                Console.Write("Cotan {0} = ", i);
                Console.WriteLine("{0:f4}", rezfun);
            }
            Console.WriteLine();
        }
    }

    class Program
    {
        public delegate void MathDelegate();
        static void Main(string[] args)
        {
            MatFunc Func = new MatFunc();
            MathDelegate SDel = Func.Sin;
            MathDelegate CDel = Func.Cos;
            MathDelegate TDel = Func.Tan;
            MathDelegate CtDel = Func.Cotan;

            MathDelegate FDelegate = SDel + CDel + TDel + CtDel;
            FDelegate();
        }
    }
}
